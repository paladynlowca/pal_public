import math
import time

import matplotlib.pyplot as plt
import numpy as np
from skimage import io, img_as_ubyte


class Element(object):
    matrix = [(10, 0), (10, 1), (9, 2), (9, 3), (9, 4), (8, 5), (8, 6), (7, 7), (6, 8), (5, 8), (4, 9), (3, 9), (2, 9),
              (1, 10)]

    def __init__(self, index: int):
        self._index = index
        self._pixels = []
        self._pixelsLocal = []
        self._slicePixels = []
        self._correlations = []
        self._checked = False
        self._dimensionsMin = (0, 0)
        self._dimensionsSize = (0, 0)
        self._bitmap = None
        self._bitmapSliced = None
        self._gravityCenter = None
        self._gravityCenterLocal = None
        self._diagonal = 0
        self._redFactor = 0
        self._value = 0

        self._blairBliss = 0
        return

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        self._index = value
        return


    @property
    def correlations(self):
        if self._checked:
            return False
        else:
            self._checked = True
            return self._correlations

    @property
    def pixels(self):
        return self._pixels

    @property
    def pixels_sliced(self):
        return self._slicePixels

    @property
    def gravity_center(self):
        return self._gravityCenter

    @property
    def diagonal(self):
        return self._diagonal

    @property
    def red_factor(self):
        return self._redFactor

    @red_factor.setter
    def red_factor(self, value):
        self._redFactor = value
        return

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        return

    def clear_pixels(self):
        self._pixels = []
        return

    def add(self, coord: tuple):
        self._pixels.append(coord)
        return

    def add_correlations(self, index: int):
        if index not in self._correlations:
            self._correlations.append(index)
            pass
        return

    def extend(self, to_add: list):
        self._pixels.extend(to_add)
        return

    def check_dimensions(self):
        i_min = self._pixels[0][0]
        j_min = self._pixels[0][1]
        i_max = self._pixels[0][0]
        j_max = self._pixels[0][1]
        for pixel in self._pixels:
            if i_min > pixel[0]:
                i_min = pixel[0]
                pass
            if i_max < pixel[0]:
                i_max = pixel[0]
                pass
            if j_min > pixel[1]:
                j_min = pixel[1]
                pass
            if j_max < pixel[1]:
                j_max = pixel[1]
                pass
            pass

        self._dimensionsMin = (i_min, j_min)
        self._dimensionsSize = (i_max - i_min + 1, j_max - j_min + 1)
        return (i_min, j_min), (i_max - i_min + 1, j_max - j_min + 1)

    def bitmap(self):
        self._bitmap = np.ndarray(shape=(self._dimensionsSize[0] + 20, self._dimensionsSize[1] + 20), dtype=np.uint8)
        self._bitmap.fill(0)
        for pixel in self._pixels:
            self._pixelsLocal.append((pixel[0] - self._dimensionsMin[0] + 10, pixel[1] - self._dimensionsMin[1] + 10))
            self._bitmap[pixel[0] - self._dimensionsMin[0] + 10][pixel[1] - self._dimensionsMin[1] + 10] = 255
            pass
        return

    def slice_pixel(self, pixel):
        for (i, j) in self.matrix:
            if not self._bitmap[pixel[0] + i][pixel[1] + j] == 255:
                return False
                pass
            if not self._bitmap[pixel[0] + i][pixel[1] - j] == 255:
                return False
                pass
            if not self._bitmap[pixel[0] - i][pixel[1] + j] == 255:
                return False
                pass
            if not self._bitmap[pixel[0] - i][pixel[1] - j] == 255:
                return False
                pass
            pass
        self._slicePixels.append((pixel[0] + self._dimensionsMin[0] - 10, pixel[1] + self._dimensionsMin[1] - 10))
        return

    def slice(self):
        for pixel in self._pixelsLocal:
            self.slice_pixel(pixel)
            pass
        self._bitmapSliced = np.ndarray(shape=(self._dimensionsSize[0] + 20, self._dimensionsSize[1] + 20),
                                        dtype=np.uint8)
        self._bitmapSliced.fill(0)
        for pixel in self._slicePixels:
            self._bitmapSliced[pixel[0] - self._dimensionsMin[0] + 10][pixel[1] - self._dimensionsMin[1] + 10] = 255
            pass
        return

    def divide(self):
        pic = Image("who cares", mode=2, image=self._bitmapSliced)
        objects = pic.objects
        areas = []
        for element in objects:
            for index, pixel in enumerate(element.pixels):
                element.pixels[index] = (pixel[0] + self._dimensionsMin[0] - 10,
                                         pixel[1] + self._dimensionsMin[1] - 10)
                pass
            element.check_dimensions()
            element.bitmap()
            element.find_gravity_center()
            element.find_diagonals()
            areas.append((element.index, element.gravity_center[0], element.gravity_center[1], element.diagonal))
            element.clear_pixels()
            pass
        for pixel in self._pixels:
            distances = {}
            for element in areas:
                distances[element[0]] = (((pixel[0] - element[1]) * (pixel[0] - element[1])+(pixel[1]-element[2]) * (pixel[1] - element[2]))/(element[3]))
                pass
            objects[min(distances, key=distances.get)].add(pixel)
            pass
        return objects

    def find_gravity_center(self):
        x_sum = 0
        y_sum = 0
        count = 0
        for x, y in self._pixels:
            x_sum += x
            y_sum += y
            count += 1
            pass
        gc = (round(x_sum / count), round(y_sum / count))
        self._gravityCenter = gc
        self._gravityCenterLocal = (gc[0] - self._dimensionsMin[0] + 10, gc[1] - self._dimensionsMin[1] + 10)
        return

    def find_diagonals(self, mode="min"):
        x, y = self._gravityCenterLocal
        cross_value = 1.4142135623730950488016887242097
        counter_cross = [1, 1]
        x_add, y_add = (1, 1)
        while self._bitmap[x + x_add][y + y_add] != 0:
            x_add += 1
            y_add += 1
            counter_cross[0] += 1
            pass
        x_add, y_add = (1, 1)
        while self._bitmap[x - x_add][y - y_add] != 0:
            x_add += 1
            y_add += 1
            counter_cross[0] += 1
            pass
        x_add, y_add = (1, 1)
        while self._bitmap[x - x_add][y + y_add] != 0:
            x_add += 1
            y_add += 1
            counter_cross[1] += 1
            pass
        x_add, y_add = (1, 1)
        while self._bitmap[x + x_add][y - y_add] != 0:
            x_add += 1
            y_add += 1
            counter_cross[1] += 1
            pass

        counter_straight = [1, 1]
        d_add = 1
        while self._bitmap[x + d_add][y] != 0:
            d_add += 1
            counter_straight[0] += 1
            pass
        d_add = 1
        while self._bitmap[x - d_add][y] != 0:
            d_add += 1
            counter_straight[0] += 1
            pass
        d_add = 1
        while self._bitmap[x][y + d_add] != 0:
            d_add += 1
            counter_straight[1] += 1
            pass
        d_add = 1
        while self._bitmap[x][y - d_add] != 0:
            d_add += 1
            counter_straight[1] += 1
            pass
        if mode == "avg":
            self._diagonal = (counter_straight[0] + counter_straight[1] + (counter_cross[0] + counter_cross[1]) * cross_value)/4
            pass
        else:
            if min(counter_straight) < min(counter_cross) * cross_value:
                self._diagonal = min(counter_straight)
                pass
            else:
                self._diagonal = min(counter_cross) * cross_value
            pass
        return

    def feret(self):
        return self._dimensionsSize[1] / self._dimensionsSize[0]

    def blair_bliss(self):
        sum = 0
        gc = (self._gravityCenter[0], self._gravityCenter[1])
        for pixel in self._pixels:
            sum += (int(gc[0]) - int(pixel[0]))*(int(gc[0]) - int(pixel[0])) + (int(gc[1]) - int(pixel[1]))*(int(gc[1]) - int(pixel[1]))
            pass
        return len(self._pixels) / math.sqrt(2 * math.pi * sum)

    def haralick(self):
        pixel_start = (0, 0)
        for p in range(self._dimensionsSize[1]):
            if self._bitmap[10][p] != 0:
                pixel_start = (10, p)
                break
        pixel = (pixel_start[0], pixel_start[1])
        iter = [(-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0)]
        back = False
        front = False
        gc = (self._gravityCenter[0] - self._dimensionsMin[0] + 10, self._gravityCenter[1] - self._dimensionsMin[1] + 10)
        sum = 0
        sum_sqr = 0
        count = 0
        end = False
        while not end:
            dist = (int(gc[0]) - int(pixel[0]))*(int(gc[0]) - int(pixel[0])) + (int(gc[1]) - int(pixel[1]))*(int(gc[1]) - int(pixel[1]))
            sum += math.sqrt(dist)
            sum_sqr += dist
            count += 1
            i = 0
            while not back:
                if self._bitmap[(iter[i][0] + pixel[0])][(iter[i][1] + pixel[1])] == 0:
                    back = True
                    pass
                i += 1
                pass
            i = 0
            while front:
                if self._bitmap[(iter[i][0] + pixel[0])][(iter[i][1] + pixel[1])] == 255:
                    front = False
                    pixel = (pixel[0] + iter[i][0],  pixel[1] + iter[i][1])
                    pass
                i += 1
                pass
            if pixel_start[0] == pixel[0] and pixel_start[1] == pixel[1]:
                end = True
                pass
            pass
        return math.sqrt((sum * sum) / (count * sum_sqr - 1))

    def __str__(self):
        return 'Obiekt numer {0} ze środkiem ciężkości w punkcie {1} ma współczynniki wynoszące: Fereta {2:.4}, Blaria-Blissa {3:.4} oraz Haralicka: {4:.4}.'.format(self._index, self._gravityCenter, self.feret(), self.blair_bliss(), self.haralick())
    pass


class Image(object):
    def __init__(self, name: str, mode: int = 1, image=None):
        if mode == 1:
            self._name = name
            self._imageOriginal = None
            self.open(name)
            self._imageBinary = np.ndarray(shape=(self._imageOriginal.shape[0], self._imageOriginal.shape[1]),
                                           dtype=self._imageOriginal.dtype)
            pass
        elif mode == 2:
            self._imageBinary = image
            pass
        self._objectCount = 0
        self._objects = []
        self._mergeObjects = []
        self._finalObjects = []
        self._pixelList = {}
        self._correlations = {}
        self._amount = 0

        self._imageSlice = np.ndarray(shape=(self._imageBinary.shape[0], self._imageBinary.shape[1]),
                                      dtype=self._imageBinary.dtype)
        self._tempBitmap = np.ndarray(shape=(self._imageBinary.shape[0], self._imageBinary.shape[1]),
                                      dtype=self._imageBinary.dtype)
        self._tempBitmap.fill(0)
        self._coins = {5: 0, 2: 0, 1: 0, 0.5: 0, 0.2: 0, 0.1: 0, 0.05: 0, 0.02: 0, 0.01: 0}

        if mode == 1:
            pass
            self.trash_holding()
            self.divide_basic()
            self.divide_merge()

            for obj in self._mergeObjects:
                obj.check_dimensions()
                obj.bitmap()
                obj.slice()
                for i, j in obj.pixels_sliced:
                    self._imageSlice[i][j] = 255
                    pass
                self._finalObjects.extend(obj.divide())
                pass
            n = 0
            for obj in self._finalObjects:
                obj.index = n
                obj.check_dimensions()
                obj.bitmap()
                obj.find_gravity_center()
                obj.find_diagonals(mode="avg")
                color = len(obj.pixels) / 20
                pixel_count = 0
                color_count = 0
                for i, j in obj.pixels:
                    pixel_count += 1
                    if (int(self._imageOriginal[i][j][2]) - int(self._imageOriginal[i][j][0])) < -30:
                        color_count += 1
                        pass
                    pass
                if color_count / pixel_count < 0.2:
                    if obj.diagonal > 64:
                        obj.value = 1
                        self._coins[1] += 1
                        pass
                    elif obj.diagonal > 57:
                        obj.value = 0.5
                        self._coins[0.5] += 1
                        pass
                    elif obj.diagonal > 51:
                        obj.value = 0.2
                        self._coins[0.2] += 1
                        pass
                    else:
                        obj.value = 0.1
                        self._coins[0.1] += 1
                        pass
                    pass
                elif color_count / pixel_count < 0.70:
                    if obj.diagonal > 68:
                        obj.value = 5
                        self._coins[5] += 1
                        pass
                    else:
                        obj.value = 2
                        self._coins[2] += 1
                        pass
                    pass
                else:
                    if obj.diagonal > 52:
                        obj.value = 0.05
                        self._coins[0.05] += 1
                        pass
                    else:
                        obj.value = 0.01
                        self._coins[0.01] += 1
                        pass
                    pass
                self._amount += obj.value
                obj.red_factor = color_count / pixel_count
                n += 1
                print(obj)
                pass
            print(self._name)
            print(self._coins, n)
            print(round(self._amount, 2))
            pass
        elif mode == 2:
            pass
            self.divide_basic()
            self.divide_merge()
            pass
        return

    def open(self, name: str):
        self._imageOriginal = io.imread(name)
        self._imageOriginal = img_as_ubyte(self._imageOriginal)
        return

    def trash_holding(self, etap=1):
        for j in range(self._imageOriginal.shape[0]):
            for i in range(self._imageOriginal.shape[1]):
                if etap == 2:
                    if (int(self._imageOriginal[j][i][2]) - int(self._imageOriginal[j][i][0])) > -30:
                        self._imageBinary[j, i] = 0
                        pass
                    else:
                        self._imageBinary[j, i] = 255
                        pass
                    pass
                else:
                    if (int(self._imageOriginal[j][i][2]) - int(self._imageOriginal[j][i][0])) > 50:
                        self._imageBinary[j, i] = 0
                        pass
                    else:
                        self._imageBinary[j, i] = 255
                        pass
                    pass
                pass
            pass
        return

    def divide_basic(self):
        for i in range(1, self._imageBinary.shape[0] - 1):
            for j in range(1, self._imageBinary.shape[1] - 1):
                local = []
                if self._imageBinary[i][j] != 0:
                    # Poszukiwanie o już przeszukanej okolicy innych części obiektu.
                    if self._imageBinary[i][j - 1] == self._imageBinary[i][j]:
                        local.append(self._pixelList[(i, j - 1)].index)
                        pass
                    if self._imageBinary[i - 1][j] == self._imageBinary[i][j]:
                        local.append(self._pixelList[(i - 1, j)].index)
                        pass
                    # Przypisywanie pikselowi klucza obiektu z sąsiedztwa, z najniższym kluczem.
                    if bool(local):
                        curent = min(local)
                        self._objects[curent].add((i, j))
                        self._pixelList[(i, j)] = self._objects[curent]
                        if len(local) == 2 and local[0] != local[1]:
                            self._objects[local[0]].add_correlations(local[1])
                            self._objects[local[1]].add_correlations(local[0])
                            pass
                        pass
                    # Lub przydzielenie mu nowego, gdy nic nie znaleziono.
                    else:
                        self._objects.append(Element(self._objectCount))
                        self._objects[self._objectCount].add((i, j))
                        self._pixelList[(i, j)] = self._objects[self._objectCount]
                        self._objectCount += 1
                        pass
                    pass
                pass
            pass
        return

    def divide_merge_part(self, checked, index):
        cor_list = self._objects[index].correlations
        if not cor_list:
            return
        else:
            checked.append(index)
            for i in cor_list:
                self.divide_merge_part(checked, i)
            return

    def divide_merge(self):
        self._objectCount = 0
        for i in range(0, len(self._objects)):
            checked = []
            self.divide_merge_part(checked, i)
            if checked:
                self._mergeObjects.append(Element(self._objectCount))
                for n in checked:
                    self._mergeObjects[self._objectCount].extend(self._objects[n].pixels)
                    pass
                self._objectCount += 1
                pass
            pass
        return

    @staticmethod
    def print(version):
        plt.figure(figsize=(20, 20))
        plt.imshow(version, cmap="gray")
        plt.axis('off')
        plt.show()

    @property
    def objects(self):
        return self._mergeObjects
    pass


img = Image('1.jpg')
# times = [('start', time.perf_counter())]
# for g in range(1, 16):
#     name = g.__str__() + '.jpg'
#     img = Image(name)
#     times.append((name, time.perf_counter()))
#     pass
# r = 0
# for t in times:
#     print(t, t[1] - r)
#     r = t[1]
#     pass
