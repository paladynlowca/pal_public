#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//kółko i krzyżyk
void space() {int i; for(i=0;i<5;i++) printf("\n\n\n\n\n\n\n\n\n\n");}

int		GAME					(int option[]);
void	IA_easy					(int signs[], int count, int player);
void	IA_medium				(int option[], int signs[], int count, int player);
int		IA_hard					(int option[], int signs[], int count, int player, int status);
int		IA_check_line			(int signs[], int *sign, int s1, int s2, int s3);
int		menu_main				(int option[], char nick[]);
void	menu_option				(int option[], char nick[]);
void	menu_sign				(int option[]);
void	menu_diff				(int option[]);
void	menu_nick				(int option[], char nick[]);
void	place_board				(int status, int signs[]);
void	place_box				(int sign, int line);
void	place_check_menu		(int signs[]);
void	place_check_menu_line	(int signs[], int line);
int		win_check				(int signs[]);
int		win_check_line			(int signs[], int s1, int s2, int s3);
int		player_turn				(int signs[], int player);

int main(void)
{
	srand(time(0));
	int option[4]={0,1,1,0};
	char nick[11];
	int check = 0;
	while(check!=-1)
	{
		check=menu_main(option, nick);
		if(check==1)
		{
			GAME(option);
		}
	}
}

GAME (int option[])
{
	int status=2, signs[10]={0,0,0,0,0,0,0,0,0,0}, player, place, count=0, IA_status;
	char c;
	place_board(status, signs);
	player=(rand()%2)*2-1;
	
	printf("\n\n\n\n\n");
	while(status==2)
	{
		if(player==option[1])
		{
			place_board(status, signs);
			printf("\nWybierz gdzie postawisz swoj znak\n");
			place_check_menu(signs);
			player_turn(signs,player);
		}
		else
		{
			switch(option[2])
			{
				case 1:
					IA_easy(signs,count,player);
					break;
				case 2:
					IA_medium(option,signs,count,player);
					break;
				case 3:
					IA_medium(option,signs,count,player);
					break;
				default:
					status=-10;
					break;
			}
		}
		count++;
		player*=-1;
		/*if(win_check(signs))
		{
			place_board(status,signs);
		}*/
		if((status=win_check(signs))!=2);
		else if(count==9)
		{
			status=0;
		}
	}
	
	place_board(status,signs);
	switch(status)
	{
		case 1:
		case -1:
			if(status==option[1])
			{
				printf("\nWygrales\n\n");
				getchar();
			}
			else
			{
				printf("\nPrzegrales\n\n");
				getchar();
			}
			break;
		case 0:
			printf("\nZremisowales\n\n");
			getchar();
			break;
		default:
			printf("Wystapil blad\n\n");
			getchar();
	}
	while((c = getchar()) != '\n' && c != EOF);
	return status;
}

void IA_easy (int signs[], int count, int player)
{
	int i, j=0;
	i=rand()%(9-count)+1;
	while(i!=0)
	{
		j++;
		if(signs[j]==0)
		{
			i--;
		}
	}
	signs[j]=player;
	return;
}

void IA_medium (int option[], int signs[], int count, int player)
{
	int i, j=0, sign_place=0;
	int line[8][3]={{1,2,3},{4,5,6},{7,8,9},{1,4,7},{2,5,8},{3,6,9},{1,5,9},{3,5,7}};
	for(i=0;i<8;i++)
	{
		if(IA_check_line(signs, &sign_place,line[i][0],line[i][1],line[i][2])==player)
		{
			signs[sign_place]=player;
			printf("11111111111111111111111111111111111111111111111111111");
			return;
		}
		printf("|||||  %d  ||||",sign_place);
	}
	if(sign_place!=0)
	{
		printf("ewgwgrhrhaerhatjartjsrjsryjryskykryksyktsyk");
		signs[sign_place]=player;
		return;
	}
	else
	{
		i=rand()%(9-count)+1;
		while(i!=0)
		{
			j++;
			if(signs[j]==0)
			{
				i--;
			}
		}
		signs[j]=player;
		return;
	}
}

int IA_hard (int option[], int signs[], int count, int player, int IA_status)
{
	int status;
	if(count==0)
	{
		signs[5]=player;
	}
	
	if(count==2&&signs[2]+signs[4]+signs[6]+signs[8]==player*-1)
	{
		if(signs[2]==player*-1)
		{
			status=101;
			signs[1]=player;
		}
		if(signs[4]==player*-1)
		{
			status=101;
			signs[1]=player;
		}
		if(signs[6]==player*-1)
		{
			status=109;
			signs[9]=player;
		}
		if(signs[8]==player*-1)
		{
			status=109;
			signs[9]=player;
		}
	}
	if(status==101)
	{
		if(signs[9]!=player*-1)
		{
			signs[9]==player;
		}
		if(signs[9]==player*-1)
		{
			
		}
	}
	return status;
}

int	IA_check_line		(int signs[], int *sign, int s1, int s2, int s3)
{
	if(signs[s1]+signs[s2]+signs[s3]==2)
	{
		if(signs[s1]==0)
		{
			*sign=s1;
		}
		else if(signs[s2]==0)
		{
			*sign=s2;
		}
		else if(signs[s3]==0)
		{
			*sign=s3;
		}
		return 1;
	}
	else if(signs[s1]+signs[s2]+signs[s3]==-2)
	{
		if(signs[s1]==0)
		{
			*sign=s1;
		}
		else if(signs[s2]==0)
		{
			*sign=s2;
		}
		else if(signs[s3]==0)
		{
			*sign=s3;
		}
		return -1;
	}
	else
	{
		return 0;
	}
}

int menu_main			(int option[], char nick[])
{
	char w, c;
	int i;
	
	while(1)
	{
		space();
		if(option[3]==1)
		{
			printf("\n===============\nWitaj %s",nick);
		}
		printf("\n------------------\nMenu glowne\n\nN - Nowa gra\nO - Opcje\nQ - Wyjscie\n");
		printf("\nWybierz jedna z opcji\n");
		for(i=0;i<10;i++)
		{
			scanf(" %c",&w);
			if(w=='N'||w=='n')
			{
				return 1;
			}
			else if (w=='O'||w=='o')
			{
				menu_option(option,nick);
				i=10;
			}
			else if (w=='Q'||w=='q')
			{
				return -1;
			}
			else
			{
				printf("\nNie ma takiej opcji, sproboj ponownie.\n");
			}
			while((c = getchar()) != '\n' && c != EOF);
		}
	}
}

void menu_option		(int option[], char nick[])
{
	int i;
	char w, c;
	while (1)
	{
		space();
		printf("\n------------------\nMenu opcji\n\n1 - Wybierz symbol\n2 - Poziom trodnosci\n3 - Pseudonim\nQ - Powrot\n");
		printf("\nWybierz jedna z opcji\n");
		
		for(i=0;i<10;i++)
		{
			scanf(" %c",&w);
			if(w=='1')
			{
				menu_sign(option);
				i=10;
			}
			else if (w=='2')
			{
				menu_diff(option);
				i=10;
			}
			else if (w=='3')
			{
				menu_nick(option, nick);
				i=10;
			}
			else if (w=='Q'||w=='q')
			{
				return;
			}
			else
			{
				printf("\nNie ma takiej opcji, sproboj ponownie.\n");
			}
			while((c = getchar()) != '\n' && c != EOF);
		}
	}
}

void menu_sign			(int option[])
{
	int i;
	char w, c;
	while(1)
	{
		space();
		printf("\n------------------\nMenu opcji - wybor symbolu\n\nX - Wybierz krzyzyk\nO - Wybierz kolko\nQ - Powrot\n");
		printf("\nWybierz symbol\n");
		
		for(i=0;i<10;i++)
		{
			scanf(" %c",&w);
			if(w=='X'||w=='x')
			{
				option[1]=-1;
				printf("\nWybrano krzyzyk\n");
				return;
			}
			else if (w=='O'||w=='o')
			{
				option[1]=1;
				printf("\nWybrano kolko\n");
				return;
			}
			else if (w=='Q'||w=='q')
			{
				return;
			}
			else
			{
				printf("\nNie ma takiej opcji, sproboj ponownie.\n");
			}
			while((c = getchar()) != '\n' && c != EOF);
		}
	}
}

void menu_diff			(int option[])
{
	int i;
	char w, c;
	while(1)
	{
		space();
		printf("\n------------------\nMenu opcji - wybor poziomu trudnosci\n\n1 - Latwy\n2 - Sredni\nQ - Powrot\n"); /*\n3 - Trudny*/
		printf("\nWybierz symbol\n");
		
		for(i=0;i<10;i++)
		{
			scanf(" %c",&w);
			if(w=='1')
			{
				option[2]=1;
				printf("\nWybrano niski poziom trudnosci.\n");
				return;
			}
			else if (w=='2')
			{
				option[2]=2;
				printf("\nWybrano sredni poziom trudnosci.\n");
				return;
			}
			/*else if (w=='3')
			{
				option[2]=3;
				printf("\nWybrano wysoki poziom trudnosci.\n");
				return;
			}*/
			else if (w=='Q'||w=='q')
			{
				return;
			}
			else
			{
				printf("\nNie ma takiej opcji, sproboj ponownie.\n");
			}
			while((c = getchar()) != '\n' && c != EOF);
		}
	}
}

void menu_nick			(int option[], char nick[])
{
	int i;
	char w, c;
	char nick_temp[21];
	while(1)
	{
		space();
		printf("\n------------------\nMenu opcji - wybor pseudonimu\nWpisz pseudonim\nQ - Powrot\n");
		printf("\nWpisz pseudonim (max 20 znakow)\n");
		scanf(" %s", nick_temp);
		if (!(strcmp(nick_temp,"q")||!strcmp(nick_temp,"Q")))
			{
				return;
			}
		nick_temp[21]=0;
		strcpy(nick,nick_temp);
		option[3]=1;
		return;
	}
}

void place_check_menu	(int signs[])
{
	printf("\n\n-------------");
	place_check_menu_line(signs, 1);
	printf("\n-------------");
	place_check_menu_line(signs, 4);
	printf("\n-------------");
	place_check_menu_line(signs, 7);
	printf("\n-------------\n\n");
}

void place_check_menu_line(int signs[], int line)
{
	int i=line+3;
	printf("\n|");
	for(1;line<i;line++)
	{
		if(signs[line]==0)
		{
			printf(" %d ", line);
		}
		else
		{
			printf("   ");
		}
		printf("|");
	}
}

void place_board		(int status, int signs[])
{
	int i, j, k;
	space();
	printf("\n+--------------+--------------+--------------+");
	for (i=0;i<7;i=i+3)
	{
		for (j=0;j<7;j++)
		{
			printf("\n|");
			for(k=1;k<4;k++)
			{
				place_box(signs[i+k],j);
			}
		}
		printf("\n+--------------+--------------+--------------+");
	}
}

void place_box			(int sign, int line)
{
	char sign_x[7][16]= 		{{"              |"},{"  X        X  |"},{"    X    X    |"},{"      XX      |"},{"    X    X    |"},{"  X        X  |"},{"              |"}};		
	char sign_o[7][16]= 		{{"              |"},{"     OOOO     |"},{"   O      O   |"},{"  O        O  |"},{"   O      O   |"},{"     OOOO     |"},{"              |"}};
	char sign_x_win[7][16]= 	{{"$$$$$$$$$$$$$$|"},{"$ X        X $|"},{"$   X    X   $|"},{"$     XX     $|"},{"$   X    X   $|"},{"$ X        X $|"},{"$$$$$$$$$$$$$$|"}};		
	char sign_o_win[7][16]= 	{{"$$$$$$$$$$$$$$|"},{"$    OOOO    $|"},{"$  O      O  $|"},{"$ O        O $|"},{"$  O      O  $|"},{"$    OOOO    $|"},{"$$$$$$$$$$$$$$|"}};
	char sign_none[7][16]= 		{{"              |"},{"              |"},{"              |"},{"              |"},{"              |"},{"              |"},{"              |"}};
	
	if(sign==0)
	{
		printf("%s", sign_none[line]);
	}
	else if(sign==1)
	{
		printf("%s", sign_o[line]);
	}
	else if(sign==-1)
	{
		printf("%s", sign_x[line]);
	}
	else if(sign==2)
	{
		printf("%s", sign_o_win[line]);
	}
	else if(sign==-2)
	{
		printf("%s", sign_x_win[line]);
	}
	return;
}

int win_check			(int signs[])
{
	int board_status=0;
	if(board_status=win_check_line(signs,1,2,3));
	else if(board_status=win_check_line(signs,4,5,6));
	else if(board_status=win_check_line(signs,7,8,9));
	else if(board_status=win_check_line(signs,1,4,7));
	else if(board_status=win_check_line(signs,2,5,8));
	else if(board_status=win_check_line(signs,3,6,9));
	else if(board_status=win_check_line(signs,1,5,9));
	else if(board_status=win_check_line(signs,3,5,7));
	else board_status=2;
	return board_status;
}

int	win_check_line		(int signs[], int s1, int s2, int s3)
{
	int win=signs[s1];
	if((signs[s1]+signs[s2]+signs[s3])==3||(signs[s1]+signs[s2]+signs[s3])==-3)
	{
		signs[s1]*=2;
		signs[s2]*=2;
		signs[s3]*=2;
		return win;
	}
	else
	{
		return 0;
	}
}

int player_turn			(int signs[], int player)
{
	int place;
	char c;
	while(1)
	{
		scanf("%d", &place);
		if(place>9||place<1)
		{
			printf("\nWartosc spoza zakresu, podaj poprawna.\n");
		}
		else if(signs[place]!=0)
		{
			printf("Miejsce zajete, podaj wolne.");
		}
		else if(signs[place]==0)
		{
			signs[place]=player;
			return 1;
		}
		
		while((c = getchar()) != '\n' && c != EOF);
	}
}

