//lista uporządkowana

#include <stdio.h>
#include <stdlib.h>

struct list
{
	int klucz;
	int id;
	struct list *next;
	struct list *prev;
};

struct list* dodaj(struct list* poz, int klucz, int id)
{
	struct list *poz_next = (struct list*)malloc(sizeof(struct list));
	poz_next->klucz=klucz;
	poz_next->id=id;
	poz->next=poz_next;
	poz=poz_next;
	
}

void dodaj_id(int pozid,struct list* start, int klucz, int id)
{
	struct list *poz=start, *poz_next, *poz_nn;
	if(pozid<1)
	{
		printf("za malo");
		return;
	}
	int i;
	for(i=1;i<pozid;i++)
	{
		if(poz->next==0)
		{
			printf("za duzo");
			return;
		}
		poz=poz->next;
	}
	poz_next = (struct list*)malloc(sizeof(struct list));
	poz_next->klucz=klucz;
	poz_next->id=id;
	poz_nn=poz->next;
	poz->next=poz_next;
	poz_next->next=poz_nn;
	
}

void clear_id(int pozid,struct list* start)
{
	struct list *poz=start, *poz_n, *poz_nn;
	if(pozid<1)
	{
		printf("za malo");
		return;
	}
	int i;
	for(i=1;i<pozid;i++)
	{
		if(poz->next==0)
		{
			printf("za duzo");
			return;
		}
		poz=poz->next;
	}
	poz_n=poz->next;
	poz_nn=poz_n->next;
	poz->next=poz_nn;
	free(poz_n);
	
}

void clear(struct list *start)
{
	
	struct list *poz;
	struct list *clear=start;
	do
	{
		poz=clear->next;
		free(clear);
		clear=poz;
	}
	while(poz->next!=0);
	free(clear);
}

int main(void)
{
	struct list *start, *poz;
	int i;
	start=(struct list*)malloc(sizeof(struct list));
	start->klucz=20;
	start->id=0;
	poz=start;
	for(i=1;i<=200;i++)
	{
		poz->next=dodaj(poz,20+i,i);
		poz=poz->next;
	}
	dodaj_id(83,start,44,44);
	clear_id(44,start);
	poz=start;
	printf("\n%d %d",poz->id, poz->klucz);
	while(poz->next!=0)
	{
		poz=poz->next;
		printf("\n%d %d",poz->id, poz->klucz);
	}
	clear(start);
}