#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//jeżeli wartośc początku zakresu jest wyższa od wartości końca zamienia je miejscami
void zakr_min(float *s, float *k)
{
	float t;
	if(*k<*s)
	{
		t=*k;
		*k=*s;
		*s=t;
	}
}

//pobiera od użytkownika wszystkie potrzebne współczynniki
void wyznacz_wsp(float *a, float *b, float *c, float *s, float *k, int *n)
{
	printf("Podaj wspolczynniki a, b, c\n");
	scanf(" %f %f %f", a, b, c); 
	
	printf("\nPodaj zakres\n");
	scanf(" %f %f", s, k);
	
	printf("\nPodaj kroki\n");
	scanf(" %d", n);
	
	zakr_min(s, k);
	
	return;
}

int zapis_bin(float *a, float *b, float *c, float *s, float *k, int *n)
{
	FILE *plik = fopen("f_kwadrat.bin","wb");
	char id[6] = "mcbin";
	float w, t, x;
	int i = *n;
	
	fwrite(id,sizeof(char),5,plik);
	t = (*k-*s) / (*n-1);
	
	for(;i>0;i--)
	{
		x = *s+t*(*n-i);
		w = (*a*x*x+*b*x+*c);
		float tab[5]={*a,*b,*c,w,x};
		fwrite(tab,sizeof(float),5,plik);
	}
	fclose(plik);
	return 1;
}

int zapis_txt(float *a, float *b, float *c, float *s, float *k, int *n)
{
	FILE *plik = fopen("f_kwadrat.txt","w");
	float w, t, x;
	int i = *n;
	fprintf (plik, "mctxt\n");
	t = (*k-*s) / (*n-1);
	
	for(;i>0;i--)
	{
		x = *s+t*(*n-i);
		w = (*a * x * x + *b * x + *c);
	fprintf (plik, "%6.2fx^2 +%6.2fx +%6.2f = %8.2f dla x =%6.2f\n", *a, *b, *c, w, x);
	}
	fclose(plik);
	return 1;
}

//funkcja kierująca do odpowiedniego sposobu zapisu
//zmienna ster przyjmuje wartości: 0 - nie określono, pytamy się użytkownika; 1 - zapis binarny; 2 - zapis tekstowy; inna wartość - też pytamy(ale nie stosować)
int zapis(int ster, float *a, float *b, float *c, float *s, float *k, int *n)
{
	char z;
	
	switch (ster)
	{
		case 1:
		{
			zapis_bin(a,b,c,s,k,n);
			return 1;
		}
		case 2:
		{
			zapis_txt(a,b,c,s,k,n);
			return 1;
		}
		default:
		{
			printf("\nW jaki sposob chcesz zapisac plik?\n B - Binarny\n T - tekstowy?\n");
			while(1)
			{
				scanf(" %c",&z);
				if(z=='B'||z=='b')
				{
					printf("Wybrano zapis binarny\n");
					zapis_bin(a,b,c,s,k,n);
					return 1;
				}
				else if (z=='T'||z=='t')
				{
					printf("Wybrano zapis tekstowy\n");
					zapis_txt(a,b,c,s,k,n);
					return 1;
				}
				else
				{
					printf("Nie ma takiej opcji, podaj poprawna.\n");
				}
				while((z = getchar()) != '\n' && z != EOF);
			}
		}
	}
}

// Funkcja określająca rodzaj zapsu do pliku na podstawie parametrów podanych przy wywołaniu.
int zapis_ext(char *typ, float *a, float *b, float *c, float *s, float *k, int *n)
{
	if(!strcmp(typ, "txt"))
	{
		zapis(2, a, b, c, s, k, n);
	}
	else if(!strcmp(typ, "bin"))
	{
		zapis(1, a, b, c, s, k, n);
	}
	else
	{
		zapis(0, a, b, c, s, k, n);
	}
}

int odczyt_bin(char *src)
{
	FILE *plik = fopen(src,"rb");
	char id[6];
	fread(id,sizeof(char),5,plik);
	
	float tab[5];
	fread(tab,sizeof(float),5,plik);

	while(!feof (plik))
	{
		printf("%6.2fx^2 +%6.2fx +%6.2f = %8.2f dla x =%6.2f\n", tab[0], tab[1], tab[2], tab[3], tab[4]);
		fread(tab,sizeof(float),5,plik);
	}
	fclose(plik);
	return 1;
}

int odczyt_txt(char *src)
{
	FILE *plik = fopen(src,"r");
	char text[50], *test;
	test = fgets(text,50,plik);
	
	while(1)
	{
		test = fgets(text,50,plik);
		if(test != NULL)
		{
			printf("%s",text);
			if(feof (plik))
			{
				fclose(plik);
				return 1;
			}
		}
		else
		{
			fclose(plik);
			return 1;
		}
	}
	fclose(plik);
	return 1;
}

// Funkcja kierująca do odpowiedniego sposobu odczytu.
int odczyt(char *nazwa)
{
	int i;
	char c, test[5], bin_id[5] = {'m','c','b','i','n'}, txt_id[5] = {'m','c','t','x','t'};
	FILE *plik;
	
	if((plik = fopen(nazwa, "rb"))==NULL)
	{
		printf("\nNie da sie otworzyc pliku");
		fclose(plik);
		return 0;
	}
	else
	{
		fread(test,sizeof(char),5,plik);
		fclose(plik);
		if((test[0]==bin_id[0])&&(test[1]==bin_id[1])&&(test[2]==bin_id[2])&&(test[3]==bin_id[3])&&(test[4]==bin_id[4]))
		{
			odczyt_bin(nazwa);
			printf("Binarka");
			return 1;
		}
		else if((test[0]==txt_id[0])&&(test[1]==txt_id[1])&&(test[2]==txt_id[2])&&(test[3]==txt_id[3])&&(test[4]==txt_id[4]))
		{
			odczyt_txt(nazwa);
			printf("Textowka");
			return 1;
		}
		else
		{
			printf("\nPlik nieobslugiwany.\n");
			return 0;
		}
	}
}

int main(int argc, char *argv[])
{
	float a, b, c, s, k, x, t; //a, b, c - współczynniki funkcji; s, k - początek i koniec zakresu do obliczenia; t - krok wyliczany później
	int i, n; // i - zmienna iteracyjna; n - liczba kroków
	
	switch (argc)
	{
		case 1: // Nie podano żadnego argumentu, użytkownik podaje potrzebne wartości.
		{
			wyznacz_wsp(&a, &b, &c, &s, &k, &n);
			t = (k-s)/(n-1);
			zapis(0, &a, &b, &c, &s, &k, &n);
			return 1;
		}
	
		case 2: // Podano nazwę pliku do odczytu, jako jedyny argument.
		{
			odczyt(argv[1]);
			return 1;
		}		
		case 7: // Podano argumenty ładowane jako dane wyliczeniowe.
		{
			a = strtof(argv[1],NULL);
			b = strtof(argv[2],NULL);
			c = strtof(argv[3],NULL);
			s = strtof(argv[4],NULL);
			k = strtof(argv[5],NULL);
			n = strtof(argv[6],NULL);
			
			zakr_min(&s, &k);
			t = (k-s)/(n-1);
			zapis(0, &a, &b, &c, &s, &k, &n);
			return 1;
		}		
		case 8: // Podano argumenty ładowane jako dane wyliczeniowe oraz adres pliku do zapisu.
		{
			a = strtof(argv[1],NULL);
			b = strtof(argv[2],NULL);
			c = strtof(argv[3],NULL);
			s = strtof(argv[4],NULL);
			k = strtof(argv[5],NULL);
			n = strtof(argv[6],NULL);
			
			zakr_min(&s, &k);
			t = (k-s)/(n-1);
			zapis_ext(argv[7], &a, &b, &c, &s, &k, &n);
			return 1;
		}	
		default: // Gdy podano nieobsługiwaną ilość parametrów.
		{
			printf("Podano niewlasciwa liczbe argumentow.\nWprowadz cokolwiek, aby zakonczyc.");
			char c;
			scanf("%c",&c);
			return 0;
		}
	}
}